import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.manager.audit.AuditService;
import org.kie.api.runtime.manager.audit.ProcessInstanceLog;
import org.kie.api.runtime.manager.audit.VariableInstanceLog;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;

import com.redhat.bpms.examples.mortgage.Application;


public class MortgageDemoService {
	
	// jBPM Process and Project constants
	private final String P_EMPLOYEE = "erics";
	private final String P_RESULT = "result";
	private final String T_APPROVAL_VAR = "_approval";
	
	/**
	 * El deployment_id se visualiza desde el business-central en 
	 * Deploy --> Process deployment
	 * Está compuesto por  datos de maven:
	 * groupID+artifactID+version
	 * 
	 */
	
	private final String DEPLOYMENT_ID = "com.redhat.bpms.examples:mortgage:1.5";
	private final String USERNAME = "erics";
	private final String PASSWORD = "bpmsuite1!";
	
	/**
	 * El process_id se visualiza en el business-cental en 
	 * Authoring --> Project Authoring --> Business Process
	 * MortgageApplication
	 * En el diseño del proceso, click sobre el fondo blanco, ventanita de propiedades
	 * Propiedades centrales --> ID
	 */
	private final String PROCESS_ID = "com.redhat.bpms.examples.mortgage.MortgageApplication";
	private final String SERVER_URL = "http://localhost:8080/business-central";
	
	// jBPM classes
	private RuntimeEngine engine;
	private KieSession ksession;
	private TaskService taskService;
	private AuditService auditService;

	private static MortgageDemoService instance = null;

	private MortgageDemoService() throws MalformedURLException {
		engine = RemoteRuntimeEngineFactory
				.newRestBuilder()
				.addDeploymentId(DEPLOYMENT_ID).addUserName(USERNAME)
				.addPassword(PASSWORD).addUrl(new URL(SERVER_URL)).build();
		taskService = engine.getTaskService();
		ksession = engine.getKieSession();
		auditService = engine.getAuditService();
	}
	
	public void doTask(long taskId, boolean approve) {
		Map<String, Object> params = new HashMap<>();
		params.put(T_APPROVAL_VAR, approve);
		taskService.claim(taskId, USERNAME);
		taskService.start(taskId, USERNAME);
		taskService.complete(taskId, USERNAME, params);
	}
	
	/**
	 *Inicia un nuevo proceso de negocio MortgageDemo
	 */
	
	public void startProcess(String employeeName, Application app) {
		Map<String, Object> params = new HashMap<>();
		
		//Se pasa al iniciar el proceso los valores del modelo Applicant.
		params.put(P_EMPLOYEE, employeeName);
		params.put("application", app);
		ksession.startProcess(PROCESS_ID, params);
		
	}
	
	public List<String> getAllProcessesSummary() {

		List<? extends ProcessInstanceLog> piLogs = auditService.findProcessInstances(PROCESS_ID);
		List<String> processSummary = new ArrayList<>();

		for (ProcessInstanceLog processInstanceLog : piLogs) {
			processSummary.add(getProcessSummary(processInstanceLog));
		}
		
		return processSummary;
	}

	public String getEmployeeName(long piid) {
		return getVariableValue(piid, P_EMPLOYEE);
	}

	public void clearHistory() {
		auditService.clear();
	}

	public static MortgageDemoService getInstance()
			throws MalformedURLException {
		if (instance == null) {
			instance = new MortgageDemoService();
		}
		return instance;
	}

	public String getVariableValue(long piid, String varName) {
		String value = null;
		List<? extends VariableInstanceLog> variables = auditService
				.findVariableInstances(piid, varName);
		if (variables.size() > 0)
			value = variables.get(0).getValue();
		return value;
	}
	
	private String getProcessSummary(ProcessInstanceLog pi) {
		long piid = pi.getProcessInstanceId();
		String employee = getVariableValue(piid, P_EMPLOYEE);
		String result = getVariableValue(piid, P_RESULT);
		String status = "";
		switch (pi.getStatus()) {
		case ProcessInstance.STATE_ABORTED:
			status = "aborted";
			break;
		case ProcessInstance.STATE_ACTIVE:
			status = "active";
			break;
		case ProcessInstance.STATE_COMPLETED:
			status = "completed";
			break;
		case ProcessInstance.STATE_PENDING:
			status = "pending";
			break;
		case ProcessInstance.STATE_SUSPENDED:
			status = "suspended";
			break;
		default:
			status = "unknown";
			break;
		}
		
		if (result == null || result.length() == 0 ){
			result = "reward still waiting for approval";			
		}
		
		String summary = "Reward process for employee '%s' is %s and result is '%s'.";
		return String.format(summary, employee, status, result);
	}
	
}
