import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

import com.redhat.bpms.examples.mortgage.Applicant;
import com.redhat.bpms.examples.mortgage.Application;
import com.redhat.bpms.examples.mortgage.Appraisal;
import com.redhat.bpms.examples.mortgage.Property;

/**
 * Este pequeño ejemplo está basado en la demo de Eric Schabell
 * https://github.com/jbossdemocentral/bpms-mortgage-demo
 * Lo que se hace en este cliente es usar la API RemoteRuntimeEngineFactory (Utilizando rest
 * por debajo) para iniciar una nueva instancia de proceso, obtener un sumario de todos los procesos
 * y obtener el valor de una variable de una instancia de proceso en particular. 
 * 
 * 
 * @author gsoria
 *
 */
public class TestClient {
	
	public static void main(String[] args) {
		
		MortgageDemoService service = null;
		
		//Se crean datos de ejemplo
		Applicant applicant = new Applicant();
		applicant.setCreditScore(12000);
		applicant.setIncome(2000);
		applicant.setName("Pedro Perez");
		applicant.setSsn(5000);
		
		Property p = new Property();
		p.setAddress("Peru 1055 casi artigas");
		p.setPrice(12000);
		
		Appraisal appraisal = new Appraisal();
		appraisal.setDate(null);
		appraisal.setValue(100);
		appraisal.setProperty(p);
		
		Application app = new Application();
		app.setAmortization(120000);
		app.setApplicant(applicant);
		app.setAppraisal(appraisal);
		app.setApr((double) 1235);
		app.setDownPayment(12000);
		app.setMortgageAmount(1000);
		app.setProperty(p);
		
		
		//Iniciar una nueva instancia de proceso MortgageDemo
		try {
			service = MortgageDemoService.getInstance();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		if (service != null){
			service.startProcess("erics", app);
		}

		
		//Obtener la lista completa de instancias de procesos
		List <String> allTasks = service.getAllProcessesSummary();
		for (Iterator<String> iterator = allTasks.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			System.out.println(string);
		}
		
		//Obtener el valor de una variable, de una instancia de proceso en particular
		System.out.println(service.getVariableValue(2L, "downPayment"));
		
	}

}
